#include <Arduino.h>
#include "part/ButtonPad.h"
#include "game/SpeedPush.h"

#include <Wire.h>

long iter=0;
long lastIter=0;


ButtonPad pad = ButtonPad();
SpeedPush speedPush= SpeedPush(&pad);
GameStep *currentGame = &speedPush;


void setup() 
{
  Wire.begin();
  Serial.begin(115200);
  Serial.println("Starting Setup...");
  pad.setup();
  speedPush.init();
  Serial.println("Setup Complete.");

}

void loop() {
  currentGame->run();
  pad.scan();
  if(millis() - lastIter > 1000){
    lastIter=millis();
    iter ++;
  }
  
}


