

#include "WifiStatusDisplayContent.h"
#include "../lib/display/Display.h"

WifiStatusDisplayContent::WifiStatusDisplayContent(Wifi *wifi) : wifi(wifi) {}

void WifiStatusDisplayContent::fill(TFT_eSPI *tft) {
    Serial.println("show wifi status");
    this->currentDisplay = wifi->isConnected();
    if (!this -> currentDisplay) {
        Serial.println("show not connected");
        tft->fillCircle(4, 4, 2, Display::RED);
    }
}

bool WifiStatusDisplayContent::valid() {
    return this->currentDisplay == wifi->isConnected();
}

