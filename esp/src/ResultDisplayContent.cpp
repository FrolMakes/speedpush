

#include "ResultDisplayContent.h"
#include "../lib/display/Display.h"
#include "../lib/qrcode/QRCodeTFTPrinter.h"

ResultDisplayContent::ResultDisplayContent(int score, string url) : score(score), url(url) {}

void ResultDisplayContent::fill(TFT_eSPI *tft) {

    Serial.println(("Result added : " + to_string(score) + " : " + url ).c_str());
    tft->setRotation(0);
    tft->fillScreen(Display::WHITE);
    tft->setTextColor(Display::BLACK, Display::WHITE);
    tft->setTextSize(3);
    tft->setCursor(30, 160-43, 2);
    tft->println(score);

    QRCodeContent code = QRCodeContent(url);
    QRCodeTFTPrinter::paint(tft, &code, 118, 5, 5);
}
