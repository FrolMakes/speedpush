

#include "ScoreBigDisplayContent.h"
#include "../lib/display/Display.h"

ScoreBigDisplayContent::ScoreBigDisplayContent(int score) : score(score) {}

void ScoreBigDisplayContent::fill(TFT_eSPI *tft) {
    tft->setRotation(3);
    tft->fillScreen(Display::WHITE);
    tft->setTextColor(Display::BLACK, Display::WHITE);
    tft->setTextSize(6);
    tft->setCursor(8, 20, 2);
    tft->println(score);
}