

#pragma once


#include "../lib/display/DisplayContent.h"

class ScoreBigDisplayContent : public DisplayContent{
public:
    ScoreBigDisplayContent(int score=0);
    void fill(TFT_eSPI* display) override;
private:
    int score;

};



