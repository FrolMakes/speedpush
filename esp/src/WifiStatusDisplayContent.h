

#pragma once


#include <Wifi.h>
#include "../lib/display/DisplayContent.h"

class WifiStatusDisplayContent : public DisplayContent{
private:
    Wifi* wifi;
    bool currentDisplay = false;
public:
    WifiStatusDisplayContent(Wifi *wifi);

    void fill(TFT_eSPI *display) override;
    bool valid();

};



