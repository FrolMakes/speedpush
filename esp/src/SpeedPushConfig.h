//
// Created by francois on 2022-04-27.
//

#pragma once

#include <Config.h>

class SpeedPushConfig : public Config {
public:
    const char *ssid() {
        return Config::get("ssid");
    }

    const char *wifiPassword() {
        return Config::get("pass");
    }

    const char *hostname() {
        return Config::get("hostname");
    }

    const char* backend(){
        return Config::get("backendUrl");
    }

    const char* authorization(){
        return Config::get("authorization");
    }
};


