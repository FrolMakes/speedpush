#include <Arduino.h>
#include <TFT_eSPI.h>       // Hardware-specific library

#include <ArduinoJson.h>
#include <SPIFFS.h>
#include <Wire.h>
#include <Wifi.h>
#include <SpeedPushConfig.h>
#include <HTTPClient.h>
#include <string>
#include "../lib/qrcode/QRCodeContent.h"
#include "../lib/qrcode/QRCodeTFTPrinter.h"
#include "ResultDisplayContent.h"
#include "ScoreBigDisplayContent.h"
#include "WifiStatusDisplayContent.h"
#include "../lib/display/LayeredContent.h"
#include "../lib/display/Display.h"


using namespace std;

TFT_eSPI tft;

Wifi wifi;
SpeedPushConfig config;
int score = 0;
int scoreToPush = 0;

ResultDisplayContent resultDisplayContent;
ScoreBigDisplayContent scoreBigDisplayContent;
WifiStatusDisplayContent wifiStatusDisplayContent(&wifi);
LayeredContent displayContent(&wifiStatusDisplayContent, &scoreBigDisplayContent);
Display display(&tft, &displayContent);



void show(DisplayContent *content);

void receiveEvent(int howMany) {

    unsigned int x = 0;
    x |= Wire.read();
    x |= Wire.read() << 8;

    Serial.print("score");
    Serial.println(x);
    if (x != 0) {
        scoreBigDisplayContent = ScoreBigDisplayContent(x);
        show(&scoreBigDisplayContent);
    } else {
        scoreToPush = score;
    }
    score = x;
}

void show(DisplayContent *content) {
    displayContent.l2 = content;
    display.invalidateContent();
}

string readUrl(string result){
    DynamicJsonDocument jsonDocument(2048);
    deserializeJson(jsonDocument,result);
    const char * r = jsonDocument["url"];
    return string(r);
}


void sendScore(int score) {
    if(wifi.isConnected()){
        HTTPClient http;
        string url = string(config.backend()) + "/api/score/SpeedPush/add/" + to_string(scoreToPush);
        http.begin(url.c_str());
        http.addHeader("Authorization", config.authorization());
        int returnCode = http.GET();
        string resp = string(http.getString().c_str());
        Serial.println(resp.c_str());
        if(returnCode == 200 && resp.size() > 2){
            resultDisplayContent = ResultDisplayContent(score, readUrl(resp));
            show(&resultDisplayContent);
            scoreToPush = 0;
        } else {
            Serial.println(("Error while submitting score : " + to_string(returnCode) + " : " + resp).c_str());
        }
    }
}


void setup() {
    Serial.begin(115200);

    //has to be in main.cpp or fail
    if (!SPIFFS.begin()) {
        throw std::runtime_error("An Error has occurred while mounting SPIFFS");
    }
    config.setup();

    Wire.begin(4);
    Wire.onReceive(receiveEvent);

    tft.init();

    wifi.setup(config.ssid(), config.wifiPassword(), config.hostname());


    pinMode(33, OUTPUT);
    digitalWrite(33, HIGH);


}

void loop() {
    wifi.run();
    if (scoreToPush != 0) {
        sendScore(scoreToPush);
    }
    if(!wifiStatusDisplayContent.valid()){
        display.invalidateContent();
    }
    display.run();
    delay(10);
}