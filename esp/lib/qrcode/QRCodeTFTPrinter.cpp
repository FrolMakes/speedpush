

#include "QRCodeTFTPrinter.h"

void QRCodeTFTPrinter::paint(TFT_eSPI *display, QRCodeContent *qrCode, int sizePx, int xOffset, int yOffset) {
    int sizeInSquares = qrCode->sizeInSquare();
    Serial.print(" size in square ");
    Serial.println(sizeInSquares);
    int squareSize = sizePx/sizeInSquares;
    int xRealOffset = xOffset + (sizePx - sizeInSquares*squareSize)/2 ;
    int yRealOffset = yOffset + (sizePx - sizeInSquares*squareSize)/2 ;

    for(int x = 0; x < sizeInSquares; x++){
        for(int y = 0; y < sizeInSquares; y++) {
            display->fillRect(xRealOffset + x * squareSize, yRealOffset + y*squareSize, squareSize, squareSize,
                             (qrCode->fill(x,y))?TFT_BLACK:TFT_WHITE
                             );
        }
    }
}