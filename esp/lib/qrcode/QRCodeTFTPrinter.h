

#pragma once
#include <string>
#include "QRCodeContent.h"
#include <TFT_eSPI.h>       // Hardware-specific library

using namespace std;
class QRCodeTFTPrinter {
public:
    static void paint(TFT_eSPI *display, QRCodeContent* qrCode,int sizePx,int xOffset = 0,int yOffset = 0);
};




