

#include "QRCodeContent.h"

const int QRCodeContent::VERSION_MAX_ALPHA_ECC_MEDIUM[] = {20,38,61,90,122,154,178,221,262,311,366,419,483,528,600,656,734,816,909,970,1035,1134,1248,1326,1451,1542,1637,1732,1839,1994,2113,2238,2369,2506,2632,2780,2894,3054,3220,3391};

QRCodeContent::QRCodeContent(string content): content(content) {

    int v = 0;
    do{
        v++;
    } while(content.size() >= VERSION_MAX_ALPHA_ECC_MEDIUM[v-1] || v >= 40);
    version = v+1; // +1 should not be there but withtout it the qr code are not read
    qrcodeBytes = (uint8_t*) malloc(qrcode_getBufferSize(version));
    qrcode_initText(&qrcode, qrcodeBytes,version, ECC_MEDIUM, content.c_str());
}

QRCodeContent::~QRCodeContent() {
    free(qrcodeBytes);
}


bool QRCodeContent::fill(int x, int y) {
    return qrcode_getModule(&qrcode, x, y);
}
