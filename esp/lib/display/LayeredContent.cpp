

#include "LayeredContent.h"

LayeredContent::LayeredContent(DisplayContent *l1, DisplayContent *l2) : l1(l1), l2(l2){

}

void LayeredContent::fill(TFT_eSPI *display) {
    l2->fill(display);
    l1->fill(display);
}
