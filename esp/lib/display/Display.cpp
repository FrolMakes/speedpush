

#include "Display.h"

Display::Display(TFT_eSPI *tft, DisplayContent *content) : tft(tft), content(content) {}

void Display::run() {
    if(!contentValid && content){
        this->contentValid=true;
        tft->fillScreen(Display::WHITE);
        content->fill(tft);
    }
}

void Display::invalidateContent() {
    this->contentValid = false;

}
