

#pragma once


#include "DisplayContent.h"

class LayeredContent : public DisplayContent {
public:
    LayeredContent(DisplayContent* l1, DisplayContent* l2);
    DisplayContent *l1;
    DisplayContent *l2;

    void fill(TFT_eSPI *display) override;

};



